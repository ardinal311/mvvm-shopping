package com.ardinal.shopping.util;

import com.ardinal.shopping.model.entity.Auth;
import com.orhanobut.hawk.Hawk;

public class Preferences {

    private static final String PREF_TOKEN = "TOKEN";

    public static void setUserInfo(Auth data) {
        Hawk.put(PREF_TOKEN, data.getToken());
    }

    public static String getUserInfo() {
        return Hawk.get(PREF_TOKEN);
    }
}