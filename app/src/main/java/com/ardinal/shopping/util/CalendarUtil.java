package com.ardinal.shopping.util;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.net.IDN;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

public class CalendarUtil implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private EditText dField;
    private int dDay;
    private int dMonth;
    private int dYear;
    private Context dContext;
    private final Calendar c = Calendar.getInstance(TimeZone.getDefault());


    public CalendarUtil(Context context, EditText field) {
        Activity activity = (Activity) context;
        this.dField = field;
        this.dContext = context;
        dField.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(dContext, this, year, month, day);
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        updateField();
    }

    private void updateField() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());
        dField.setText(sdf.format(c.getTime()));
    }
}
