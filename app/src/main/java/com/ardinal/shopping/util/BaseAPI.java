package com.ardinal.shopping.util;

import com.ardinal.shopping.BuildConfig;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseAPI {

    public static Retrofit getRequest(boolean withTokenAuth) {
        return Objects.requireNonNull(getRetrofitBuilder(withTokenAuth)).build();
    }

    private static Retrofit.Builder getRetrofitBuilder(boolean withTokenAuth) {
        try {
            Retrofit.Builder retroBuilder = new Retrofit.Builder();
            OkHttpClient.Builder httpBuilder = new OkHttpClient.Builder();

            if (withTokenAuth) {
                String token = Preferences.getUserInfo();

                httpBuilder.addInterceptor(chain -> {
                    Request original = chain.request();
                    Request.Builder reqBuilder = original.newBuilder()
                            .addHeader("Authorization", token)
                            .method(original.method(), original.body());
                    Request request = reqBuilder.build();
                    return chain.proceed(request);
                });
            }

            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            httpBuilder.addInterceptor(interceptor);

            httpBuilder
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS);

            retroBuilder
                    .client(httpBuilder.build())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(BuildConfig.BASE_URL);

            return retroBuilder;
        }
        catch (Throwable e) {
            return null;
        }
    }
}
