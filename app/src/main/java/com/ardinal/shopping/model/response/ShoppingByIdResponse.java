package com.ardinal.shopping.model.response;

import com.ardinal.shopping.model.entity.Shopping;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShoppingByIdResponse{

	@SerializedName("data")
	@Expose
	private Shopping data;

	@SerializedName("message")
	@Expose
	private String message;

	public void setData(Shopping data){
		this.data = data;
	}

	public Shopping getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	@Override
 	public String toString(){
		return 
			"ShoppingByIdResponse{" + 
			"data = '" + data + '\'' + 
			",message = '" + message + '\'' + 
			"}";
		}
}