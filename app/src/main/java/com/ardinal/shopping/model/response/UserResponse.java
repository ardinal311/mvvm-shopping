package com.ardinal.shopping.model.response;

import com.ardinal.shopping.model.entity.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserResponse {
    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<User> data;

    public UserResponse() {
    }

    public UserResponse(String message, List<User> data) {
        this.message = message;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public List<User> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
