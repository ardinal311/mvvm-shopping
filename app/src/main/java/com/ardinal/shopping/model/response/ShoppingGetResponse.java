package com.ardinal.shopping.model.response;

import com.ardinal.shopping.model.entity.Shopping;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShoppingGetResponse {

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<Shopping> data;

    public ShoppingGetResponse() {
    }

    public ShoppingGetResponse(String message, List<Shopping> data) {
        this.message = message;
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public List<Shopping> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
