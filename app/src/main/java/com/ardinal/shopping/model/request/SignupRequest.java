package com.ardinal.shopping.model.request;

import com.ardinal.shopping.model.entity.User;
import com.google.gson.annotations.SerializedName;

public class SignupRequest {
    @SerializedName("user")
    User users;

    public SignupRequest(User users) {
        this.users = users;
    }
}
