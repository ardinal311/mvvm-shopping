package com.ardinal.shopping.model.request;

import com.google.gson.annotations.SerializedName;

public class SigninRequest {
    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    public SigninRequest(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
