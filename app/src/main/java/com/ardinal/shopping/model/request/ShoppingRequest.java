package com.ardinal.shopping.model.request;

import com.ardinal.shopping.model.entity.Shopping;
import com.google.gson.annotations.SerializedName;

public class ShoppingRequest{

	@SerializedName("shopping")
	private Shopping shopping;

	public ShoppingRequest(Shopping shopping) {
		this.shopping = shopping;
	}
}