package com.ardinal.shopping.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Shopping {

	@SerializedName("createddate")
	@Expose
	private String createddate;

	@SerializedName("name")
	@Expose
	private String name;

	@SerializedName("id")
	@Expose
	private int id;

	public static class Builder {
		private String createddate;
		private String name;
		private int id;

		public Shopping.Builder setCreatedDate(String createdDate) {
			this.createddate = createdDate;
			return this;
		}

		public Shopping.Builder setName(String name) {
			this.name = name;
			return this;
		}

		public Shopping.Builder setId(int id) {
			this.id = id;
			return this;
		}

		public Shopping build() {
			return new Shopping(this);
		}
	}

	private Shopping() {}

	private Shopping(Shopping.Builder builder) {
		createddate = builder.createddate;
		name = builder.name;
		id = builder.id;
	}

	public String getCreateddate(){
		return createddate;
	}

	public String getName(){
		return name;
	}

	public int getId(){
		return id;
	}

	@Override
 	public String toString(){
		return 
			"Shopping{" +
			"createddate = '" + createddate + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			"}";
		}
}