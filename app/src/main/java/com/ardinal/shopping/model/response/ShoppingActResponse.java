package com.ardinal.shopping.model.response;

import com.ardinal.shopping.model.entity.Shopping;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ShoppingActResponse {

	@SerializedName("data")
	@Expose
	private Shopping data;

	public void setData(Shopping data){
		this.data = data;
	}

	public Shopping getData(){
		return data;
	}

	@Override
 	public String toString(){
		return 
			"ShoppingActResponse{" +
			"data = '" + data + '\'' + 
			"}";
		}
}