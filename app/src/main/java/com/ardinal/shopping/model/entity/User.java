package com.ardinal.shopping.model.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("Id")
    @Expose
    private int id;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("Email")
    @Expose
    private String mEmail;

    @SerializedName("encrypted_password")
    @Expose
    private String encryptedPassword;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("country")
    @Expose
    private String country;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("postcode")
    @Expose
    private String postCode;

    public static class Builder {
        private String username;
        private String email;
        private String encryptedPassword;
        private String phone;
        private String address;
        private String city;
        private String country;
        private String name;
        private String postCode;

        public User.Builder setUsername(String username) {
            this.username = username;
            return this;
        }

        public User.Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public User.Builder setEncryptedPassword(String encryptedPassword) {
            this.encryptedPassword = encryptedPassword;
            return this;
        }

        public User.Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public User.Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public User.Builder setCity(String city) {
            this.city = city;
            return this;
        }

        public User.Builder setCountry(String country) {
            this.country = country;
            return this;
        }

        public User.Builder setName(String name) {
            this.name = name;
            return this;
        }

        public User.Builder setPostCode(String postCode) {
            this.postCode = postCode;
            return this;
        }

        public User build() {
            return new User(this);
        }
    }

    public User() {
        super();
    }

    public User(User.Builder builder) {
        this.username = builder.username;
        this.email = builder.email;
        this.encryptedPassword = builder.encryptedPassword;
        this.phone = builder.phone;
        this.address = builder.address;
        this.city = builder.city;
        this.country = builder.country;
        this.name = builder.name;
        this.postCode = builder.postCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }

    public String getmEmail() {
        return mEmail;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getEncryptedPassword() {
        return encryptedPassword;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public String getName() {
        return name;
    }

    public String getPostCode() {
        return postCode;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", encryptedPassword='" + encryptedPassword + '\'' +
                ", phone='" + phone + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", name='" + name + '\'' +
                ", postCode='" + postCode + '\'' +
                '}';
    }
}
