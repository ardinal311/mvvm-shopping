package com.ardinal.shopping.api;

import com.ardinal.shopping.model.entity.Shopping;
import com.ardinal.shopping.model.request.ShoppingRequest;
import com.ardinal.shopping.model.request.SigninRequest;
import com.ardinal.shopping.model.request.SignupRequest;
import com.ardinal.shopping.model.response.ShoppingActResponse;
import com.ardinal.shopping.model.response.ShoppingByIdResponse;
import com.ardinal.shopping.model.response.ShoppingGetResponse;
import com.ardinal.shopping.model.response.UserAuthResponse;
import com.ardinal.shopping.model.response.UserResponse;

import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ShoppingAPI {

    @POST("signup")
    Observable<UserAuthResponse> postSignUp (@Body SignupRequest user);

    @POST("signin")
    Observable<UserAuthResponse> postSignIn (@Body SigninRequest user);

    @GET("users")
    Observable<UserResponse> getAllUser();

    @POST("shopping")
    Observable<ShoppingActResponse> createShopping(@Body ShoppingRequest shopping);

    @GET("shopping")
    Observable<ShoppingGetResponse> getAllShopping();

    @GET("shopping/{id}")
    Observable<ShoppingByIdResponse> getShoppingByID(@Path("id") int id);

    @PUT("shopping/{id}")
    Observable<ShoppingActResponse> updateShopping(@Path("id") int id, @Body ShoppingRequest shopping);

    @DELETE("shopping/{id}")
    Observable<ShoppingActResponse> deleteShopping(@Path("id") int id);
}
