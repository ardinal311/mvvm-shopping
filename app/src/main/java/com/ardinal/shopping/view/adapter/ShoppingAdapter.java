package com.ardinal.shopping.view.adapter;

import android.app.Activity;
import android.arch.lifecycle.LifecycleOwner;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ardinal.shopping.R;
import com.ardinal.shopping.model.entity.Shopping;
import com.ardinal.shopping.view.ShoppingUpdateActivity;
import com.ardinal.shopping.viewmodel.ShoppingDeleteViewModel;

import java.util.List;

public class ShoppingAdapter extends RecyclerView.Adapter<ShoppingAdapter.ShoppingViewHolder> {

    private List<Shopping> shoppings;
    private Context context;

    private ShoppingDeleteViewModel deleteViewModel;

    class ShoppingViewHolder extends RecyclerView.ViewHolder {

        private ShoppingViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        private void bindData(Shopping shop) {
            TextView name = itemView.findViewById(R.id.mShopping_name);
            TextView id = itemView.findViewById(R.id.mShopping_id);
            TextView date = itemView.findViewById(R.id.mShopping_date);

            TextView delete = itemView.findViewById(R.id.mShopping_delete);
            TextView update = itemView.findViewById(R.id.mShopping_update);

            Activity act = (Activity) itemView.getContext();
            delete.setOnClickListener(v -> deleteShopping(act, shop.getId()));

            update.setOnClickListener(v -> gotoUpdateShopping(act, shop.getId()));

            name.setText(shop.getName());
            id.setText(String.valueOf(shop.getId()));
            date.setText(String.valueOf(shop.getCreateddate()));
        }
    }

    public ShoppingAdapter(List<Shopping> shops, Context context) {
        this.shoppings = shops;
        this.context = context;
    }

    @NonNull
    @Override
    public ShoppingViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ShoppingViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_shopping_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ShoppingViewHolder shoppingViewHolder, int i) {
        shoppingViewHolder.bindData(shoppings.get(i));
    }

    @Override
    public int getItemCount() {
        return shoppings.size();
    }

    public void deleteShopping(Activity context, int id) {
        deleteViewModel = new ShoppingDeleteViewModel(context.getApplication(), id);
        deleteViewModel.getDeleteShoppingResponse().observe((LifecycleOwner) context, response -> {
            if (response != null) {
                Toast.makeText(context, response.getData().toString(), Toast.LENGTH_SHORT).show();
                context.recreate();
            }
        });
    }

    public void gotoUpdateShopping(Activity activity, int id) {
        Intent intent = new Intent(activity, ShoppingUpdateActivity.class);

        Bundle bundle = new Bundle();
        bundle.putInt("ID_SHOPPING", id);

        intent.putExtras(bundle);

        activity.startActivity(intent);
    }
}