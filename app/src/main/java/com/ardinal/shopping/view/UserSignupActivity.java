package com.ardinal.shopping.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.ardinal.shopping.R;
import com.ardinal.shopping.model.entity.Auth;
import com.ardinal.shopping.model.entity.User;
import com.ardinal.shopping.util.Preferences;
import com.ardinal.shopping.viewmodel.SignupViewModel;
import com.orhanobut.hawk.Hawk;

public class UserSignupActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    SignupViewModel signupViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_sign_up);

        Hawk.init(getApplicationContext()).build();

        Button mButton = findViewById(R.id.mSignUp_button);
        mButton.setOnClickListener(view -> validateSignup());
    }

    private void validateSignup() {
        TextInputEditText mUsername = findViewById(R.id.mSignUp_username);
        TextInputEditText mName = findViewById(R.id.mSignUp_name);
        TextInputEditText mEmail = findViewById(R.id.mSignUp_email);
        TextInputEditText mPhone = findViewById(R.id.mSignUp_phone);
        TextInputEditText mPassword = findViewById(R.id.mSignUp_password);
        TextInputEditText mAddress = findViewById(R.id.mSignUp_address);
        TextInputEditText mCity = findViewById(R.id.mSignUp_city);
        TextInputEditText mCountry = findViewById(R.id.mSignUp_country);
        TextInputEditText mPostCode = findViewById(R.id.mSignUp_postcode);

        String username = String.valueOf(mUsername.getText());
        String name = String.valueOf(mName.getText());
        String email  = String.valueOf(mEmail.getText());
        String password = String.valueOf(mPassword.getText());
        String phone = String.valueOf(mPhone.getText());
        String address = String.valueOf(mAddress.getText());
        String city = String.valueOf(mCity.getText());
        String country = String.valueOf(mCountry.getText());
        String postcode = String.valueOf(mPostCode.getText());

        User input = new User.Builder()
                .setUsername(username)
                .setName(name)
                .setEmail(email)
                .setEncryptedPassword(password)
                .setPhone(phone)
                .setAddress(address)
                .setCity(city)
                .setCountry(country)
                .setPostCode(postcode)
                .build();

//        Log.d(TAG, "validateSignup: "
//                + input.getUsername()
//                + input.getName()
//                + input.getEmail()
//                + input.getEncryptedPassword()
//                + input.getAddress()
//                + input.getCity()
//                + input.getCountry()
//                + input.getPostCode()
//        );

        insertUser(input);
    }

    private void insertUser(User users) {
        signupViewModel = new SignupViewModel(this.getApplication(), users);
//        signupViewModel = ViewModelProviders.of(this, new SignupViewModelFactory(this.getApplication(), users)).get(SignupViewModel.class);
        signupViewModel.getSignupResponse().observe(this, mainResponse -> {
            if (mainResponse != null) {
                Auth data = new Auth(mainResponse.getToken());
                Preferences.setUserInfo(data);
                gotoShopping();
            }
        });
    }

    private void gotoShopping() {
        Intent intent = new Intent(this, ShoppingMainActivity.class);
        startActivity(intent);
    }
}
