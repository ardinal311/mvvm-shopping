package com.ardinal.shopping.view;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.ardinal.shopping.R;
import com.ardinal.shopping.model.entity.Shopping;
import com.ardinal.shopping.util.CalendarUtil;
import com.ardinal.shopping.viewmodel.ShoppingCreateViewModel;

public class ShoppingCreateActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    ShoppingCreateViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_create);


        EditText dateField = findViewById(R.id.mShopCreate_date);
        Button mButton = findViewById(R.id.mShopCreate_button);
        CalendarUtil cal = new CalendarUtil(this, dateField);

        mButton.setOnClickListener(view -> createShopping());
    }

    private void createShopping() {
        TextInputEditText mName = findViewById(R.id.mShopCreate_name);
        EditText dateField = findViewById(R.id.mShopCreate_date);

        String name = String.valueOf(mName.getText());
        String date = String.valueOf(dateField.getText());

        Shopping data = new Shopping.Builder()
                .setCreatedDate(date)
                .setName(name)
                .build();

        insertShopping(data);
    }

    private void insertShopping(Shopping data) {
        viewModel = new ShoppingCreateViewModel(this.getApplication(), data);
        viewModel.getCreateShoppingResponse().observe(this, response -> {
            if (response != null) {
                Toast.makeText(this, response.getData().toString(), Toast.LENGTH_SHORT).show();
                gotoShoppingList();
            }
        });
    }

    private void gotoShoppingList() {
        Intent intent = new Intent(this, ShoppingGetAllActivity.class);
        startActivity(intent);
    }
}
