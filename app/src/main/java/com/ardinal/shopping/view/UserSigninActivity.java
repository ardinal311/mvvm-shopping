package com.ardinal.shopping.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.ardinal.shopping.R;
import com.ardinal.shopping.model.entity.Auth;
import com.ardinal.shopping.model.entity.User;
import com.ardinal.shopping.util.Preferences;
import com.ardinal.shopping.viewmodel.SigninViewModel;
import com.orhanobut.hawk.Hawk;

public class UserSigninActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    SigninViewModel signinViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_sign_in);
        Hawk.init(getApplicationContext()).build();

        Button mSignIn = findViewById(R.id.mSignIn_button);
        mSignIn.setOnClickListener(view -> validate());
    }

    private void validate() {
        TextInputEditText mEmail = findViewById(R.id.mSignIn_email);
        TextInputEditText mPassword = findViewById(R.id.mSignIn_password);

        String email = String.valueOf(mEmail.getText());
        String password = String.valueOf(mPassword.getText());

        User input = new User.Builder()
                .setEmail(email)
                .setEncryptedPassword(password)
                .build();

        loginAction(input.getEmail(), input.getEncryptedPassword());
    }

    private void loginAction(String email, String password) {
        signinViewModel = new SigninViewModel(this.getApplication(), email, password);
//        signinViewModel = ViewModelProviders.of(this, new SigninViewModelFactory(this.getApplication(), email, password)).get(SigninViewModel.class);
        signinViewModel.getSigninResponse().observe(this, mainResponse -> {
            if (mainResponse != null) {
                Auth data = new Auth(mainResponse.getToken());
                Preferences.setUserInfo(data);
                gotoShopping();
            }
        });
    }

    private void gotoShopping() {
        Intent intent = new Intent(this, ShoppingMainActivity.class);
        startActivity(intent);
    }
}