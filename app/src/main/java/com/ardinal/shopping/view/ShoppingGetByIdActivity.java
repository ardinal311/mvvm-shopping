package com.ardinal.shopping.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.ardinal.shopping.R;
import com.ardinal.shopping.model.response.ShoppingByIdResponse;
import com.ardinal.shopping.model.response.ShoppingGetResponse;
import com.ardinal.shopping.view.adapter.ShoppingAdapter;
import com.ardinal.shopping.view.adapter.ShoppingByIDAdapter;
import com.ardinal.shopping.viewmodel.ShoppingGetByIDViewModel;

public class ShoppingGetByIdActivity extends AppCompatActivity {

    private ShoppingGetByIDViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_get_by_id);

        Button mBtn = findViewById(R.id.mShopByID_button);
        mBtn.setOnClickListener(v -> getData());
    }

    private void getData() {
        EditText field = findViewById(R.id.mShopByID_key);
        String temp = String.valueOf(field.getText());
        int id = Integer.parseInt(temp);

        viewModel = new ShoppingGetByIDViewModel(this.getApplication(), id);
        viewModel.getShoppingByID().observe(this, this::showData);
    }

    private void showData(ShoppingByIdResponse response) {
        Log.d("DATA_ID", "showData: " + response.getData());
        RecyclerView recyclerView = findViewById(R.id.mShopByID_recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        RecyclerView.Adapter mAdapter = new ShoppingByIDAdapter(response.getData(), this);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }
}
