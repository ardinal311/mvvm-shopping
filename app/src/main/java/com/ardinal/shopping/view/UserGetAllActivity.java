package com.ardinal.shopping.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ardinal.shopping.R;
import com.ardinal.shopping.view.adapter.UserAdapter;
import com.ardinal.shopping.model.response.UserResponse;
import com.ardinal.shopping.viewmodel.GetAllUsersViewModel;
import com.orhanobut.hawk.Hawk;

public class UserGetAllActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    GetAllUsersViewModel getAllUsersViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_get_all);

        Hawk.init(getApplicationContext()).build();

        getAllUsersViewModel = new GetAllUsersViewModel(this.getApplication());
        getAllUsersViewModel.getAllUsersResponse().observe(this, this::showData);
    }

    private void showData(UserResponse userResponse) {
        RecyclerView recyclerView = findViewById(R.id.mUser_recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        RecyclerView.Adapter mAdapter = new UserAdapter(userResponse.getData());
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }
}
