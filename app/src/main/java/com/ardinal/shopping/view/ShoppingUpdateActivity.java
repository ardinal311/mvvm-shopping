package com.ardinal.shopping.view;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.ardinal.shopping.R;
import com.ardinal.shopping.model.entity.Shopping;
import com.ardinal.shopping.model.request.ShoppingRequest;
import com.ardinal.shopping.util.CalendarUtil;
import com.ardinal.shopping.viewmodel.ShoppingUpdateViewModel;

import org.w3c.dom.Text;

public class ShoppingUpdateActivity extends AppCompatActivity {

    private ShoppingUpdateViewModel viewModel;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_update);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            this.id = bundle.getInt("ID_SHOPPING");
        }

        EditText dateField = findViewById(R.id.mShopUpdate_date);
        Button mButton = findViewById(R.id.mShopUpdate_button);
        CalendarUtil cal = new CalendarUtil(this, dateField);

        TextView txtID = findViewById(R.id.mShopUpdate_id);
        txtID.setText(String.valueOf(id));

        mButton.setOnClickListener(view -> updateShopping());
    }

    private void updateShopping() {
        EditText dateField = findViewById(R.id.mShopUpdate_date);
        TextInputEditText mName = findViewById(R.id.mShopUpdate_name);
        TextView mId = findViewById(R.id.mShopUpdate_id);

        String date = String.valueOf(dateField.getText());
        String name = String.valueOf(mName.getText());
        int id = Integer.parseInt(String.valueOf(mId.getText()));

        Shopping shop = new Shopping.Builder()
                .setId(id)
                .setCreatedDate(date)
                .setName(name)
                .build();

        ShoppingRequest req = new ShoppingRequest(shop);

        viewModel = new ShoppingUpdateViewModel(this.getApplication(), id, req);
        viewModel.getUpdateShoppingResponse().observe(this, shoppingActResponse -> {
            onBackPressed();
        });
    }
}
