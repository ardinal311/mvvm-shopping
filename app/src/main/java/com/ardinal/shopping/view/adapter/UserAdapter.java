package com.ardinal.shopping.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ardinal.shopping.R;
import com.ardinal.shopping.model.entity.User;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {

    private List<User> userList;

    class UserViewHolder extends RecyclerView.ViewHolder {

        private UserViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        private void bindData(User user) {
            TextView name = itemView.findViewById(R.id.mUser_name);
            TextView id = itemView.findViewById(R.id.mUser_id);
            TextView username = itemView.findViewById(R.id.mUser_username);
            TextView email = itemView.findViewById(R.id.mUser_email);
            TextView phone = itemView.findViewById(R.id.mUser_phone);
            TextView address = itemView.findViewById(R.id.mUser_address);
            TextView city = itemView.findViewById(R.id.mUser_city);
            TextView country = itemView.findViewById(R.id.mUser_country);
            TextView postcode = itemView.findViewById(R.id.mUser_postcode);

            name.setText(user.getName());
            id.setText(String.valueOf(user.getId()));
            username.setText(user.getUsername());
            email.setText(user.getmEmail());
            phone.setText(user.getPhone());
            address.setText(user.getAddress());
            city.setText(user.getCity());
            country.setText(user.getCountry());
            postcode.setText(user.getPostCode());
        }
    }

    public UserAdapter(List<User> users) {
        this.userList = users;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new UserViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_user_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder userViewHolder, int i) {
        userViewHolder.bindData(userList.get(i));
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }
}



