package com.ardinal.shopping.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.ardinal.shopping.R;
import com.ardinal.shopping.util.Preferences;
import com.orhanobut.hawk.Hawk;

public class ShoppingMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_main);
        Hawk.init(getApplicationContext()).build();

        if (Preferences.getUserInfo().equals("")) {
            finish();
        }

        Button mGetAllUser = findViewById(R.id.mBtn_getAllUser);
        mGetAllUser.setOnClickListener(view -> gotoAllUser());

        Button mCreateShopping = findViewById(R.id.mBtn_createNewShopping);
        mCreateShopping.setOnClickListener(view -> gotoCreateShopping());

        Button mAllShopping = findViewById(R.id.mBtn_getAllShopping);
        mAllShopping.setOnClickListener(view -> gotoAllShopping());

        Button mByIDShopping = findViewById(R.id.mBtn_getShoppingByID);
        mByIDShopping.setOnClickListener(view -> gotoShoppingByID());
    }

    private void gotoAllUser() {
        Intent intent = new Intent(this, UserGetAllActivity.class);
        startActivity(intent);
    }

    private void gotoCreateShopping() {
        Intent intent = new Intent(this, ShoppingCreateActivity.class);
        startActivity(intent);
    }

    private void gotoAllShopping() {
        Intent intent = new Intent(this, ShoppingGetAllActivity.class);
        startActivity(intent);
    }

    private void gotoShoppingByID() {
        Intent intent = new Intent(this, ShoppingGetByIdActivity.class);
        startActivity(intent);
    }
}
