package com.ardinal.shopping.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.ardinal.shopping.R;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button mSignUp = findViewById(R.id.mBtn_signup);
        mSignUp.setOnClickListener(view -> gotoSignUp());

        Button mSignIn = findViewById(R.id.mBtn_signin);
        mSignIn.setOnClickListener(view -> gotoSignIn());
    }

    private void gotoSignUp() {
        Intent intent = new Intent(this, UserSignupActivity.class);
        startActivity(intent);
    }

    private void gotoSignIn() {
        Intent intent = new Intent(this, UserSigninActivity.class);
        startActivity(intent);
    }
}
