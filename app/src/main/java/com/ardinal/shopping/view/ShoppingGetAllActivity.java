package com.ardinal.shopping.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.ardinal.shopping.R;
import com.ardinal.shopping.model.response.ShoppingGetResponse;
import com.ardinal.shopping.view.adapter.ShoppingAdapter;
import com.ardinal.shopping.viewmodel.ShoppingGetAllViewModel;

public class ShoppingGetAllActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    ShoppingGetAllViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_get_all);

        viewModel = new ShoppingGetAllViewModel(this.getApplication());
        viewModel.getAllShoppingResponse().observe(this, this::showData);
    }

    private void showData(ShoppingGetResponse response) {
        RecyclerView recyclerView = findViewById(R.id.mShopAll_recyclerView);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        RecyclerView.Adapter mAdapter = new ShoppingAdapter(response.getData(), this);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }
}
