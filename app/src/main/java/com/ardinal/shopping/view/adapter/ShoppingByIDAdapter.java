package com.ardinal.shopping.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ardinal.shopping.R;
import com.ardinal.shopping.model.entity.Shopping;

public class ShoppingByIDAdapter extends RecyclerView.Adapter<ShoppingByIDAdapter.ShoppingByIDViewHolder> {

    private Shopping shoppings;
    private Context context;


    class ShoppingByIDViewHolder extends RecyclerView.ViewHolder {

        private ShoppingByIDViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        private void bindData(Shopping shop) {
            TextView name = itemView.findViewById(R.id.mShopping_name);
            TextView id = itemView.findViewById(R.id.mShopping_id);
            TextView date = itemView.findViewById(R.id.mShopping_date);

            TextView delete = itemView.findViewById(R.id.mShopping_delete);
            TextView update = itemView.findViewById(R.id.mShopping_update);

            delete.setVisibility(View.GONE);
            update.setVisibility(View.GONE);
        }
    }

    public ShoppingByIDAdapter(Shopping shops, Context context) {
        this.shoppings = shops;
        this.context = context;
    }

    @NonNull
    @Override
    public ShoppingByIDViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ShoppingByIDViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_shopping_item, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ShoppingByIDViewHolder holder, int i) {
        holder.bindData(shoppings);
    }

    @Override
    public int getItemCount() {
        return 0;
    }
}
