package com.ardinal.shopping.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.location.LocationListener;
import android.util.Log;

import com.ardinal.shopping.model.entity.Shopping;
import com.ardinal.shopping.model.request.ShoppingRequest;
import com.ardinal.shopping.model.response.ShoppingActResponse;
import com.ardinal.shopping.model.response.ShoppingByIdResponse;
import com.ardinal.shopping.model.response.ShoppingGetResponse;
import com.ardinal.shopping.model.response.UserAuthResponse;
import com.ardinal.shopping.model.response.UserResponse;
import com.ardinal.shopping.util.BaseAPI;
import com.ardinal.shopping.api.ShoppingAPI;
import com.ardinal.shopping.model.request.SigninRequest;
import com.ardinal.shopping.model.request.SignupRequest;
import com.ardinal.shopping.model.entity.User;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class Repository {
    private static final String TAG = Repository.class.getSimpleName();

    private MutableLiveData<UserAuthResponse> data = new MutableLiveData<>();
    private MutableLiveData<UserResponse> userData = new MutableLiveData<>();
    private MutableLiveData<ShoppingActResponse> shopData = new MutableLiveData<>();
    private MutableLiveData<ShoppingGetResponse> shopGetData = new MutableLiveData<>();
    private MutableLiveData<ShoppingByIdResponse> byIdData = new MutableLiveData<>();


    public Repository() {}

    // 1. Sign Up

    public LiveData<UserAuthResponse> getSignUpResponse(User query) {
        SignupRequest signUpRequest = new SignupRequest(query);
        BaseAPI.getRequest(false).create(ShoppingAPI.class)
                .postSignUp(signUpRequest).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserAuthResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(UserAuthResponse mainResponse) {
                        if (mainResponse != null) {
                            data.setValue(mainResponse);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        data.setValue(null);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return data;
    }

    // 2. Sign In

    public LiveData<UserAuthResponse> getSignInResponse(String email, String password) {
        SigninRequest signInRequest = new SigninRequest(email, password);
        BaseAPI.getRequest(false).create(ShoppingAPI.class)
                .postSignIn(signInRequest).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserAuthResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(UserAuthResponse mainResponse) {
                        data.setValue(mainResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        data.setValue(null);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return data;
    }

    // 3. All User

    public LiveData<UserResponse> getAllUsers() {
        BaseAPI.getRequest(true).create(ShoppingAPI.class)
                .getAllUser().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<UserResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(UserResponse userResponse) {
                        userData.setValue(userResponse);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        userData.setValue(null);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return userData;
    }

    // 4. Create Shopping

    public LiveData<ShoppingActResponse> getShoppingCreateResponse(Shopping query) {
        ShoppingRequest request = new ShoppingRequest(query);
        BaseAPI.getRequest(true).create(ShoppingAPI.class)
                .createShopping(request).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ShoppingActResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ShoppingActResponse response) {
                        shopData.setValue(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        shopData.setValue(null);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return shopData;
    }

    // 5. Get All Shopping

    public LiveData<ShoppingGetResponse> getAllShopping() {
        BaseAPI.getRequest(true).create(ShoppingAPI.class)
                .getAllShopping().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ShoppingGetResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ShoppingGetResponse response) {
                        shopGetData.setValue(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        userData.setValue(null);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
        return shopGetData;
    }

    // 6. Get Shopping By Id

    public LiveData<ShoppingByIdResponse> getShoppingByID(int id) {
        BaseAPI.getRequest(true).create(ShoppingAPI.class)
                .getShoppingByID(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ShoppingByIdResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ShoppingByIdResponse response) {
                        Log.d(TAG, "onNext: " + response.getMessage());
                        Log.d(TAG, "onNext: " + response.getData());
                        byIdData.setValue(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        byIdData.setValue(null);
                    }

                    @Override
                    public void onComplete() {

                    }
                });

        return byIdData;
    }

    // 7. Update Shopping

    public LiveData<ShoppingActResponse> updateShopping(int id, ShoppingRequest request) {
        BaseAPI.getRequest(true).create(ShoppingAPI.class)
                .updateShopping(id, request).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ShoppingActResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ShoppingActResponse response) {
                        shopData.setValue(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        shopData.setValue(null);
                    }

                    @Override
                    public void onComplete() {

                    }
                });

        return shopData;
    }

    // 8. Delete Shopping

    public LiveData<ShoppingActResponse> deleteShopping(int id) {
        BaseAPI.getRequest(true).create(ShoppingAPI.class)
                .deleteShopping(id).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ShoppingActResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ShoppingActResponse response) {
                        shopData.setValue(response);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        shopData.setValue(null);
                    }

                    @Override
                    public void onComplete() {

                    }
                });

        return shopData;
    }
}