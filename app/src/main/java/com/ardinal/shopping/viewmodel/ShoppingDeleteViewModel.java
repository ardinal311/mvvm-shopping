package com.ardinal.shopping.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.ardinal.shopping.model.response.ShoppingActResponse;
import com.ardinal.shopping.repository.Repository;

public class ShoppingDeleteViewModel extends AndroidViewModel {
    private LiveData<ShoppingActResponse> response;

    public ShoppingDeleteViewModel(@NonNull Application application, int id) {
        super(application);

        Repository shopRepository = new Repository();
        this.response = shopRepository.deleteShopping(id);
    }

    public LiveData<ShoppingActResponse> getDeleteShoppingResponse() {
        return response;
    }
}
