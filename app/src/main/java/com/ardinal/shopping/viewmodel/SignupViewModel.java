package com.ardinal.shopping.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.ardinal.shopping.model.response.UserAuthResponse;
import com.ardinal.shopping.model.entity.User;
import com.ardinal.shopping.repository.Repository;

public class SignupViewModel extends AndroidViewModel {

    private LiveData<UserAuthResponse> mainResponse;

    public SignupViewModel(@NonNull Application application, User users) {
        super(application);

        Repository authRepository = new Repository();
        this.mainResponse = authRepository.getSignUpResponse(users);
    }

    public LiveData<UserAuthResponse> getSignupResponse() {
        return mainResponse;
    }
}
