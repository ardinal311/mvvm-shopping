package com.ardinal.shopping.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.ardinal.shopping.model.response.UserAuthResponse;
import com.ardinal.shopping.repository.Repository;

public class SigninViewModel extends AndroidViewModel {
    private LiveData<UserAuthResponse> mainResponse;

    public SigninViewModel(@NonNull Application application, String email, String password) {
        super(application);

        Repository authRepository = new Repository();
        this.mainResponse = authRepository.getSignInResponse(email, password);
    }

    public LiveData<UserAuthResponse> getSigninResponse() {
        return mainResponse;
    }
}
