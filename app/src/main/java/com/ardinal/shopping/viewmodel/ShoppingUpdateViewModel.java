package com.ardinal.shopping.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.ardinal.shopping.model.request.ShoppingRequest;
import com.ardinal.shopping.model.response.ShoppingActResponse;
import com.ardinal.shopping.repository.Repository;

public class ShoppingUpdateViewModel extends AndroidViewModel {

    private LiveData<ShoppingActResponse> response;

    public ShoppingUpdateViewModel(@NonNull Application application, int id, ShoppingRequest req) {
        super(application);

        Repository shopRepository = new Repository();
        this.response = shopRepository.updateShopping(id, req);
    }

    public LiveData<ShoppingActResponse> getUpdateShoppingResponse() {
        return response;
    }
}
