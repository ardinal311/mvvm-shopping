package com.ardinal.shopping.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.ardinal.shopping.model.response.UserResponse;
import com.ardinal.shopping.repository.Repository;

public class GetAllUsersViewModel extends AndroidViewModel {
    private LiveData<UserResponse> mainResponse;

    public GetAllUsersViewModel(@NonNull Application application) {
        super(application);

        Repository authRepository = new Repository();
        this.mainResponse = authRepository.getAllUsers();
    }

    public LiveData<UserResponse> getAllUsersResponse() {
        return mainResponse;
    }
}
