package com.ardinal.shopping.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.ardinal.shopping.model.response.ShoppingByIdResponse;
import com.ardinal.shopping.model.response.ShoppingGetResponse;
import com.ardinal.shopping.repository.Repository;

public class ShoppingGetByIDViewModel extends AndroidViewModel {

    private LiveData<ShoppingByIdResponse> response;

    public ShoppingGetByIDViewModel(@NonNull Application application, int id) {
        super(application);

        Repository repo = new Repository();
        this.response = repo.getShoppingByID(id);
    }

    public LiveData<ShoppingByIdResponse> getShoppingByID() {
        return response;
    }
}
