package com.ardinal.shopping.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.ardinal.shopping.model.response.ShoppingGetResponse;
import com.ardinal.shopping.repository.Repository;

public class ShoppingGetAllViewModel extends AndroidViewModel {
    private LiveData<ShoppingGetResponse> response;

    public ShoppingGetAllViewModel(@NonNull Application application) {
        super(application);

        Repository authRepository = new Repository();
        this.response = authRepository.getAllShopping();
    }

    public LiveData<ShoppingGetResponse> getAllShoppingResponse() {
        return response;
    }
}
