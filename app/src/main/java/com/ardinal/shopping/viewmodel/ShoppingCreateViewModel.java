package com.ardinal.shopping.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.ardinal.shopping.model.entity.Shopping;
import com.ardinal.shopping.model.response.ShoppingActResponse;
import com.ardinal.shopping.repository.Repository;

public class ShoppingCreateViewModel extends AndroidViewModel {

    private LiveData<ShoppingActResponse> response;

    public ShoppingCreateViewModel(@NonNull Application application, Shopping shopping) {
        super(application);

        Repository shopRepository = new Repository();
        this.response = shopRepository.getShoppingCreateResponse(shopping);
    }

    public LiveData<ShoppingActResponse> getCreateShoppingResponse() {
        return response;
    }
}
